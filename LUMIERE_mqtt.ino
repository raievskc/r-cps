
//il faut télécharger les bibliothèques suivantes: ESP8266WiFi ou bien IoTtweetESP32 et PubSubClient

#include <IoTtweetESP32.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define wifi_ssid "WIFI_SSID"
#define wifi_password "MOT_DE_PASSE_WIFI"

#define mqtt_server "IP_MOSQUITTO"
#define mqtt_user "guest"  //s'il a été configuré sur Mosquitto
#define mqtt_password "guest" //idem


//Buffer qui permet de décoder les messages MQTT reçus
char message_buff[100];

long lastMsg = 0;   //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;  //Affiche sur la console si True



//Création des objets
    
WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(9600);     //Facultatif pour le debug
  
  setup_wifi();           //On se connecte au réseau wifi
  client.setServer(mqtt_server, 1883);    //Configuration de la connexion au serveur MQTT
  client.setCallback(callback);  //La fonction de callback qui est executée à chaque réception de message   
  
}

//Connexion au réseau WiFi
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connexion a ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'à obtenur une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 5 secondes avant de recommencer");
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  //Envoi d'un message par minute
  if (now - lastMsg > 1000 * 60) {
    lastMsg = now;
    
    //Lecture de la luminosité
    
    int LUMIERE = analogRead(A0);
    

    //afficher une erreur si le capteur ne renvoi rien
    if ( isnan(LUMIERE)) {
      Serial.println("Echec de lecture ! Verifiez votre capteur lumière");
      return;
    }
  
    if ( debug ) {
      Serial.print("Luminosité : ");
      Serial.print(LUMIERE);
 
    }  
    client.publish(topic, String(LUMIERE).c_str(), true);   //Publie la luminosité sur le topic
 
  }
  if (now - lastRecu > 100 ) {
    lastRecu = now;

  }
}
