import time
import board
import adafruit_dht
#Initialize the dht device, with the connected data pin, D17 in our case:
dhtDevice = adafruit_dht.DHT11(board.D17)
while True:
        try:
                # Print the values to the serial port
                temperature_c = dhtDevice.temperature
                humidity = dhtDevice.humidity
                print("Temp: {:.1f} C    Humidity: {}% "
                .format(temperature_c, humidity))
        except RuntimeError as error:     # Errors happen fairly often, DHT's a$
                print(error.args[0])
        time.sleep(2.0)

