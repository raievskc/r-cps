# t-CPS

Development of a resilient CPS for the thermal regulation of a building.
Using partly simulated agents and partly real ones.


Idea of a resilience scenario:
- fan is starting because outisde temperature is higher than room temperature and room temperature is lower than user-defined reference temperature.
- After some time fan-controlling agent observe that the room temperature is not increasing.
- This agent send a message to heater-controlling agent that it should start heating the room.
- It may also try to coordinate with other agents to check if it's not the room temperature sensor that is sending false data.





