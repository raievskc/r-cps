import sys
import os
sys.path.append("./class")
from NewThermostat import AsyncThermostat as Thermostat
import asyncio
import uuid

#configuration des boucle evenementiel en fonction des OSs
if 'win32' in sys.platform:
    # Windows specific event-loop policy & cmd
    asyncio.set_event_loop_policy(asyncio.WindowsProactorEventLoopPolicy())
    cmds = [['C:/Windows/system32/HOSTNAME.EXE']]
    loop = asyncio.ProactorEventLoop()
else:
    # Unix default event-loop policy & cmds
    cmds = [
        ['du', '-sh', '/Users/fredrik/Desktop'],
        ['du', '-sh', '/Users/fredrik'],
        ['du', '-sh', '/Users/fredrik/Pictures']
    ]
    loop = asyncio.get_event_loop()

client_id = str(uuid.uuid4())
topic = client_id
print("Using client_id / topic: " + client_id)
print("Starting")
asyncio.set_event_loop(loop)
loop.run_until_complete(Thermostat(loop,"127.0.0.1",client_id).main())
loop.close()
print("Finished")
