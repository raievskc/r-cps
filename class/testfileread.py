import csv
with open('some.csv', 'r', newline='') as f:
    fieldnames = ['first_name', 'last_name']
   
    reader = csv.DictReader(f, fieldnames=fieldnames)
    for row in reader:
         print(row['first_name'], row['last_name'])
