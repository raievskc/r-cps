import Agent
import board
import adafruit_dht
import time
#Initialize the dht device, with the connected data pin, D17 in our case:

class Thermometre(Agent):
    def __init__(self):
        super().__init__(self)
    
    def run(self, address, port):
        super().run(self, address, port)
        while true:
            sendmsg(self,'temp', string(tempcapture(board.D17)))
            time.sleep(5.0)
        
    def tempcapture(pin):
        dhtDevice = adafruit_dht.DHT11(pin)

        try:
                # Print the values to the serial port
                temperature_c = dhtDevice.temperature
                humidity = dhtDevice.humidity
                print("Temp: {:.1f} C    Humidity: {}% "
                .format(temperature_c, humidity))
        except RuntimeError as error:     # Errors happen fairly often, DHT's a$
                print(error.args[0])
        
    
    
