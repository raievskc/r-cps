from Agent import Agent
import time

class Chauffage(Agent):
    def __init__(self):
        super().__init__(self)
    
    def run(self, address, port):
        super().run(address, port)
        self.com.subto("temp")
        self.com.loopstart()
        
        while 1 :
            print(self.com.getdata("temp"))
            time.sleep(5.0)