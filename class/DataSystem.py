import paho.mqtt.client as mqtt
from Donnee import Donnee
import fnmatch
import os


        
class DataSystem(mqtt.Client):
	# The callback for when the client receives a CONNACK response from the server.
    

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        #self.isconnected = 'true'
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        for data in self.datalist:
            self.subscribe(data.name.split('.')[0])

    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        for data in self.datalist :
            if data.name == (msg.topic + '.csv'):
                data.addvalue(msg.payload)
                print("fichier de topic existant ajout de valeur")
                return
        print("fichier inexistant creation du fichier et de l'objet donnee")
        self.datalist.append(Donnee(msg.topic + '.csv'))
        self.datalist[-1].addvalue(msg.payload)

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))
        
    def fileresearch(self):
        #self.isconnected = 'false'
        for file in os.listdir('.'):
            if fnmatch.fnmatch(file, '*.csv'):
                self.datalist.append(Donnee(file))
        
    def on_disconnect(self, client, userdata, rc):
        print("disconnected")
        #self.isconnected = 'false'
        #address est un string et port est un entier
        
    #def on_publish(self, client, userdata, mid):
        
        
    def run(self, address, port):
        self.datalist = []
        self.fileresearch()
        #self.isconnected = "false"
        self.connect(address, port, 60)
    
    def loopstart(self):
        self.loop_forever(timeout=1.0, max_packets=1, retry_first_connection=False)

    def sendmsg(self, topic, msg, qos = 0):
        print("try to publish")
        self.publish(topic, msg, qos=0, retain=False)
        print("msg sended in"+topic+msg)

    def subto(self, topic):
        print("subto" + topic)
        for data in self.datalist:
            if data.name == topic + '.csv':
                return
        self.subscribe(topic)
         
        
    
    def getdata(self, topic):
        for data in self.datalist :
            if data.name == (topic + '.csv'):
                getbest('state a metre ici')

