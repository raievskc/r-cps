import csv
with open('some.csv', 'w', newline='') as f:
    fieldnames = ['first_name', 'last_name']
    writer = csv.DictWriter(f, fieldnames=fieldnames)

    writer.writeheader()
    writer.writerow({'first_name': 'Baked', 'last_name': 'Beans'})
    writer.writerow({'first_name': 'Lovely', 'last_name': 'Spam'})
    writer.writerow({'first_name': 'Wonderful', 'last_name': 'Spam'})
with open('some.csv', 'r', newline='') as fr:
    fieldnames = ['first_name', 'last_name']
    reader = csv.DictReader(fr, fieldnames=fieldnames)
    for row in reader:
         print(row['first_name'], row['last_name'])
