from NewAgent import AsyncAgent as Agent
import time
import asyncio

class Thermostat(Agent):
    def __init__(self, loop, address):
        super().__init__(loop, address)
        self.consigne = 25.0
    
    async def main(self):
        super().main()
        print("Start Publishing")
        self.got_message = self.loop.create_future()
        
        msg = await self.got_message
        print("Got response with {} bytes".format(len(msg)))
        self.got_message = None
        while 1 :
            self.client.publish('temp', str(self.consigne))
            await asyncio.sleep(5)
            
        #self.client.disconnect()
        #print("Disconnected: {}".format(await self.disconnected))