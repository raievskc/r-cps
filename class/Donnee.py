import csv
import time
import os
from datetime import datetime

class Donnee:
    def __init__(self, nom):
        self.name = nom
        self.data = {'date' : 'value'}
        self.fieldnames = ['date', 'value']

        if os.path.exists(nom) == False :
            with open(nom , 'w', newline='') as self.fw:   #"Save/" + 
                self.writer = csv.DictWriter(self.fw, fieldnames=self.fieldnames)
                self.writer.writeheader()
            
        with open(nom , 'r', newline='') as self.fr:
            self.reader = csv.DictReader(self.fr, fieldnames=self.fieldnames)
            for row in self.reader:
                self.data[row['date']]= row['value'] #recuperation des date et data avec leur etiquette
        
    def addvalue(self, value):
        print("debug addvalue")
        now = datetime.now().isoformat(timespec='seconds')
        self.data[now]=str(value)
        with open(self.name , 'w', newline='') as self.fw:   #"Save/" + 
            self.writer = csv.DictWriter(self.fw, fieldnames=self.fieldnames)
            for i in self.data.items(): 
                self.writer.writerow({'date': i[0], 'value': i[1]})

    
    def getbest(state):
        return self.data[-1]['value']
        #if state == 'oper' :
        #    return self.data[-1]
        #else state == 'degr' :
        #    for d,v in self.date.items():
        
        
    def dateconvert(self, date):
        da = date.split("T")
        dd = da[0].split("-")
        aa = int(dd[0])
        mm = int(dd[1])
        jj = int(dd[2])
        he = da[1].split(":")
        hh = int(he[0])
        mi = int(he[1])
        ss = int(he[2])
        
        return (aa-2020)* 31536000 + mm * 2592000 + jj * 86400 + hh * 3600 + mi * 60 + ss
    
    def heureconvert(self, date):
        da = date.split("T")
        he = da[1].split(":")
        hh = int(he[0])
        mi = int(he[1])
        ss = int(he[2])
        
        return hh * 3600 + mi * 60 + ss
