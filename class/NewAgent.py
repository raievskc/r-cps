import socket
import uuid
import paho.mqtt.client as mqtt
import asyncio
from Donnee import Donnee
import fnmatch
import os

class AsyncioHelper:
    def __init__(self, loop, client):
        self.loop = loop
        self.client = client
        self.client.on_socket_open = self.on_socket_open
        self.client.on_socket_close = self.on_socket_close
        self.client.on_socket_register_write = self.on_socket_register_write
        self.client.on_socket_unregister_write = self.on_socket_unregister_write

    def on_socket_open(self, client, userdata, sock):
        print("Socket opened")

        def cb():
            print("Socket is readable, calling loop_read")
            client.loop_read()

        self.loop.add_reader(sock, cb)
        self.misc = self.loop.create_task(self.misc_loop())

    def on_socket_close(self, client, userdata, sock):
        print("Socket closed")
        self.loop.remove_reader(sock)
        self.misc.cancel()

    def on_socket_register_write(self, client, userdata, sock):
        print("Watching socket for writability.")

        def cb():
            print("Socket is writable, calling loop_write")
            client.loop_write()

        self.loop.add_writer(sock, cb)

    def on_socket_unregister_write(self, client, userdata, sock):
        print("Stop watching socket for writability.")
        self.loop.remove_writer(sock)

    async def misc_loop(self):
        print("misc_loop started")
        while self.client.loop_misc() == mqtt.MQTT_ERR_SUCCESS:
            try:
                await asyncio.sleep(1)
            except asyncio.CancelledError:
                break
        print("misc_loop finished")


class AsyncAgent:
    def __init__(self, loop, address):
        self.loop = loop
        self.address = address

    def on_connect(self, client, userdata, flags, rc):
        print("Connected")
        for data in self.datalist:
            client.subscribe(data.name.split('.')[0])
        #client.subscribe(topic)

    def on_message(self, client, userdata, msg):
        if not self.got_message:
            print("Got unexpected message: {}".format(msg.decode()))
        else:
            self.got_message.set_result(msg.payload)

    def on_disconnect(self, client, userdata, rc):
        self.disconnected.set_result(rc)

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))
        
    def fileresearch(self):
        #self.isconnected = 'false'
        for file in os.listdir('.'):
            if fnmatch.fnmatch(file, '*.csv'):
                self.datalist.append(Donnee(file))
        
    def subto(self, topic):
        print("subto" + topic)
        for data in self.datalist:
            if data.name == topic + '.csv':
                return
        client.subscribe(topic)
    
    def sendmsg(self, topic, msg, qos = 0):
        print("try to publish")
        client.publish(topic, msg, qos=0, retain=False)
        print("msg sended in"+topic+msg)
        
    def getdata(self, topic):
        for data in self.datalist :
            if data.name == (topic + '.csv'):
                getbest('state a metre ici')
                
                
    async def main(self):
        self.disconnected = self.loop.create_future()
        self.got_message = None

        self.client = mqtt.Client(client_id=client_id)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect

        aioh = AsyncioHelper(self.loop, self.client)
        self.datalist = []
        self.fileresearch()

        self.client.connect(self.address, 1883, 60)
        self.client.socket().setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 2048)

        #self.client.disconnect()
        #print("Disconnected: {}".format(await self.disconnected))

