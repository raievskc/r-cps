import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.


# The callback for when a PUBLISH message is received from the server.


client = mqtt.Client()
client.on_connect = on_connect


client.connect("192.168.2.48", 1883, 60)

client.publish("test", "la bitabidul", qos=0, retain=False)

client.disconnect()
