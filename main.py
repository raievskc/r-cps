import sys
sys.path.append("./class")
from Thermostat import Thermostat
import asyncio

print("Starting")
loop = asyncio.get_event_loop()
loop.run_until_complete(Thermostat(loop,"127.0.0.1").main())
loop.close()
print("Finished")