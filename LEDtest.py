import RPi.GPIO as GPIO
import time
import board
import adafruit_dht
import paho.mqtt.client as paho

broker = "86.202.128.143"
port = 1883

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connection successful, Returned code=", rc)
    else:
        print("Bad connection returned code=", rc)

def on_publish(client, userdata, result):
    print("data published \n")
    pass
client1 = paho.Client("control1")
client1.on_publish = on_publish
client1.on_connect = on_connect
client1.connect(broker, port, keepalive=60, bind_address="")

def on_disconnect(client, userdata, rc):
    print("Client disconnected")
client1.on_disconnect = on_disconnect

#initialize the LED's GPIO connection:
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(19,GPIO.OUT)

#Initialize the dht device, with the connected data pin, D17 in our case:
dhtDevice = adafruit_dht.DHT11(board.D17)

while True:
        try:
                # Print the values to the serial port
                temperature_c = dhtDevice.temperature
                humidity = dhtDevice.humidity
                print("Temp: {:.1f} C    Humidity: {}% "
                .format(temperature_c, humidity))
                ret = client1.publish("test", temperature_c, 0)
                if temperature_c>25:
                    GPIO.output(19, GPIO.HIGH)
                else:
                    GPIO.output(19, GPIO.LOW)
        except RuntimeError as error:     # Errors happen fairly often, DHT's a$
                print(error.args[0])
        time.sleep(2.0)

client1.disconnect()
